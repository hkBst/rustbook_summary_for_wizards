# Summary of the Rust book for wizards

This is a summary of the introductory "Rust Programming Language" book for software wizards who are already familiar with many programming paradigms and just want to know what pieces constitute Rust. Rust is a compiled language which most closely resembles C++, but with much stricter static memory safety guarantees even in the presence of concurrency, and with some powerful features from Haskell and Scheme.

Rust is composed of the following features:
 - the base consists of C++ without inheritance, but with a Haskell-like class system,
 - compiler-guaranteed memory safety, which enforces single ownership (like resource-acquisition is initialization (RAII) from C++) and mutual exclusion (single writer but no readers, or multiple readers but no writer),
 - algebraic data types from typed functional languages (such as SML/Ocaml or Haskell), which are called enums,
 - hygienic macros like Scheme,
 - UTF-8 strings by default like modern Python.

The following paragraphs correspond to the chapters of [the "Rust Programming Language" book](https://doc.rust-lang.org/book/).

## Foreword
Rust is about empowering programmers by making memory management and concurrency safe of crashes or security holes. It is also ergonomic enough to write many other kinds of code. Skills developed using Rust in one domain transfer to other domains.

## Getting Started

### Installation
Covers installation.

### Hello, World!
In Rust, the "Hello, world!" program is:

    fn main() {
        println!("Hello, world!");
    }

If it is in a file called "main.rs", then you can compile it using `rustc main.rs`. If successful, you can then run the resulting executable. There does not seem to be any way to get a REPL (yet?). You'll probably want to use cargo to manage compilation and running though.

### Hello, Cargo!
Cargo is the package manager of the Rust ecosystem. You can use it to create a new project called _project_name_ using `cargo new project_name`. This will initialize a git repository with a Cargo.toml ([Tom’s Obvious, Minimal Language format](https://toml.io/)) and a src directory with a "Hello, world!" main.rs. See also `cargo new --help`.

You can run only the compiler checks with `cargo check`, check and build with `cargo build` (results go in the target/debug directory), and check, build and run with `cargo run`. For faster code, but longer build time, you can compile with optimizations enabled using `cargo build --release` (results go in the target/release directory).

## Programming a Guessing Game
Code for a minigame where you have to guess the correct number (updated for rand-0.8.3).

Some points to keep in mind about how Rust does things when reading the code:
 - Comments follow C++ style of line (//) and block (/* ... */) comment forms. Nested block comments are supported.
 - You have to opt in to mutability with `mut`; immutable is the default. There are also compile-time constants which use `const name : type = value`.
 - You bind (declare and assign) a _name_ to a _value_ with `let name = value;`, and assign to it using `name = value;`.
 - You can declare a name with `let name;` and later assign to it (once if immutable) with `name = value;`.
 - You have to opt out of transfering ownership when assigning by using references (&). In a function call the arguments get assigned to the parameters, so this also applies to function calls. Again immutable is the default, so use (&mut) for a mutable reference. Rust's unique ownership feature is described in one of the next chapters.
 - Rust uses its `cmp` function with 

```
use std::{io,cmp::Ordering};

use rand::Rng;

fn main() {
    println!("Guess the number!");

    const SECRET_MAX : u32 = 1000;
    let secret_number = rand::thread_rng().gen_range(1..=SECRET_MAX);

    //println!("The secret number is: {}", secret_number);

    loop {
        println!("Please input your guess.");
        
        let mut guess = String::new();
        
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");
        
        let guess: u32 = match guess.trim().parse() {
            Ok(num) if num <= SECRET_MAX => num,
            _ => {
                println!("Please type a natural number not greater than {}!", SECRET_MAX);
                continue;
            }
        };
    
        println!("You guessed: {}", guess);
        
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
```

## Common Programming Concepts
Rust has keywords, for a list see Appendix A.

### Variables and Mutability
Variables are immutable by default, but you can opt in to be mutability with `mut`. Once an immutable value has been assigned to, it cannot be changed, but it is possible to declare it without assigning it a value (`let name;`) and later assign to it once (`name = value;`). There are also compile-time constants which use `const name : type = value`.

You bind (declare and assign) a _name_ to a _value_ with `let name = value;`, and assign to it using `name = value;`. To introduce a name you have to use `let`, so there is no ambiguity between an assignment and a new binding under the same name. Thus you can introduce a new variable with the same name as an existing variable (shadowing the old binding), and assign it a new value. This looks a lot like an assignment, but existing references will continue to see the old value.

### Data Types
Rust has integer data types of both the signed and unsigned kinds, with sizes increasing by factors of 2 from 8 bits (`i8` and `u8`) to 128 bits (`i128` and `u128`). Signed numbers are stored using [two’s complement](https://en.wikipedia.org/wiki/Two%27s_complement) representation. Additionally, `isize` and `usize` can be either 32 or 64 bits wide depending on the computer architecture. In debug mode, Rust will panic if you cause overflow, but not in release mode, where instead values will wrap around. But relying on this is still considered an error. If you need this use [std::num::Wrapping](https://doc.rust-lang.org/std/num/struct.Wrapping.html).

Number syntax seems to be like in Python. Numbers can use `_` as a visual separator, and you can write them using decimal (`123_456_789`), hexadecimal (`0xff`), octal (`0o77`), and binary (`0b1111_0000`), and for 8 bit values there is byte syntax (b'A'). 

Floating-point numbers are represented according to the IEEE-754 standard. Rust has two floating-point types: `f64` (the default), and `f32`. 

Numeric operations are standard. See Appendix B.

Boolean types (`bool`) are `true` and `false`.

Char literals use single quotes, are four bytes, and represent a Unicode scalar value, which range from U+0000 to U+D7FF and U+E000 to U+10FFFF inclusive.

Rust has fixed-length tuples which consist of values of different types (`(1, 1.1, 'a')`), and tuple-destructuring (like Python and Haskell). Rust also has fixed-length arrays which consist of values of one type (`[1, 2, 3] : [i32; 3]`). You can specify an array of identical values with `[value; length]`. Indices start from 0. Arrays are stack-allocated. Vectors are more flexible than arrays, but they are heap-allocated. Array access is bounds checked either statically or dynamically, even in release mode.

### Functions
Function definition: `fn name(param : type) -> ret_type {body}`. A semicolon (;) turns an expression into a statement. If a function ends with an expression, it is returned implicitly (like in Scheme and Haskell), but you can also return explicitly.

### Comments
Comments follow C++ style of line (//) and block (/* ... */) comment forms. Nested block comments are supported.

### Control Flow
The conditional expression is `if condition { true_branch }` with an optional `else { false_branche }`. There is no automatic coercion of integers to booleans.

There are three looping constructs: `loop`, `while`, and `for`. With `break` we can escape from a loop and also determine the value of the loop, for example: `let v = loop { break 5; }; // will set v to 5`. To go to the next iteration of a loop use `continue`.

Looping through a collection is best done like so:
```
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }
}
```
You can also:
```
fn main() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
```

## Understanding Ownership
### What is Ownership
The rules of ownership:
 - Each value has a unique owner.
 - When an owner goes out of scope, its value will be dropped, freeing any resources owned (memory, file handles).

This is like resource-acquisition-is-initialization (RAII) from C++.

There are two string types which mirrors the array/vector situation. The `str` type is stack allocated and immutable, the `String` type is heap allocated and more flexible. This does mean that you need to explicitly convert from `str` to `String` if you want a literal `String`: `let s = String::from("hello");`. The `String` type is really a handle to some memory on the heap, so if you copy the handle, both handles will point to the same memory.

Demonstrating `push` and `push_str`:
```
let mut s = String::from("Hello");
s.push(','); // append a single char 
s.push_str(" world!"); // append a string
println!("{}", s); // This will print `hello, world!`
```

Demonstrating string ownership and mutability:
```
fn main() {
    let s1 = String::from("Hello"); // can be immutable
    let mut s2 = s1; // s2 is the new owner of "Hello", s1 cannot be used anymore
    s2.push_str(", world!"); // mutate the shared memory
    println!("{}", s2); // Must use s2 here, since s1 was moved
}
```
or
```
fn main() {
    let mut s1 = String::from("Hello"); // must be mutable
    let s2 = &mut s1; // s2 is just a reference, and not the new owner
    s2.push_str(", world!"); // mutate the shared memory
    println!("{}{}", s1, s1); // Can use either s1 or s2 here, but not both
}
```
or make a deep copy with `clone`:
```
fn main() {
    let mut s1 = String::from("Hello");
    let mut s2 = s1.clone(); // deep copy of "Hello"
    s1.push('1');
    s2.push('2');
    println!("s1 = {}, s2 = {}", s1, s2);
}
```

For values that are completely stack-allocated, such as integers, the rules are less strict, because there really is no ownership of heap-data to worry about:
```
fn main() {
    let x = 5;
    let y = x; // "deep" copy
    println!("x = {}, y = {}", x, y); // can use both
}
```
Types that follow these rules have the `Copy` trait, but no type can have `Copy` is any of its parts has `Drop`. See Appendix C about the `Copy` and other derivable traits. Integers, floats, booleans, and chars are `Copy`, and also tuples that consist solely of types that are `Copy`.

The semantics for passing a value to a function are similar to those for assigning a value to a variable.

Return values can also be moved out, transfering ownership.

### References and Borrowing
If we don't want to tranfer ownership into a function, we can pass it references:
```
fn main() {
    let s = String::from("Hello");
    let len = calculate_length(&s); // pass an immutable reference
    println!("The length of '{}' is {}.", s, len);
}

fn calculate_length(s: &String) -> usize {
    s.len()
}
```
Or with a mutable reference:
```
fn main() {
    let mut s = String::from("hello");
    change(&mut s); // pass a mutable reference
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```
There can be a single mutable reference but then there can be no other references, or we can have multiple immutable references.

The compiler will not simply let you pass a reference out of a function to data that goes out of scope at the end of the function (like you can in C++). Instead of a reference pass the value itself and it will be moved. Alternatively there are lifetimes, which are discussed in "Generic Types, Traits, and Lifetimes".

### The Slice Type
A slice is a reference to a subarray. Instead of keeping track of a subarray using a pair of indices, using a slice let's the compiler understand what you are doing, enabling it to check that your slice remains valid. For example, if an array is emptied, then any indices pointing into it are no longer valid and using such an index will result in a panic, but the compiler can complain about emptying an array if there is a slice referencing it.

A `&str` is a slice into a string, and literal strings are string slices. String slices must occur at valid UTF-8 character boundaries.

Example showing slice syntax:
```
    let s = String::from("hello world");

    let hello = &s[0..5];
    let world = &s[6..11];
```

Both slice indices are optional, defaulting to the beginning and length respectively.

Functions with `&String` parameters can be made more general by changing those to `&str`. The same holds for vector slices.

## Using Structs to Structure Related Data

### Defining and Instantiating Structs
A struct is like a tuple with named (instead of numbered) parts.

Example syntax:
```
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```

Example of update syntax:
```
let user2 = User {
    email: String::from("another@example.com"),
    username: String::from("anotherusername567"),
    ..user1 // other fields equal those of named instance
};
```

A _tuple struct_ or named tuple, is like a tuple with a unique type even though it may have the same fields as another tuple:

```
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);
```

You can also define a struct with no fields: `struct Unit;`.

### An Example Program Using Structs
```
#[derive(Debug)] // for formatting with {:?}
struct Rectangle {
    width: u32,
    height: u32,
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!("rect1 is {:?}", rect1);
}
```

### Method Syntax
Methods are functions defined within the context of a struct, enum, or trait. Their first parameter is always `self`. Calling a method is often easier than calling a function, because depending on the type of `self`, Rust automatically does the correct referencing for you. Thus you don't need to write `(&mut object).method()`, because `object.method()` suffices.

Example:
```
#[derive(Debug)] // for formatting with {:?}
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );
}
```

An associated function is a method with no `self` parameter, and is often used as a constructor.

## Enums and Pattern Matching
Rust’s enums are most similar to algebraic data types in functional languages, such as F#, OCaml, and Haskell.

### Defining an Enum
Example syntax showing the generic Option enum, which is like [OCaml's Option](https://caml.inria.fr/pub/docs/manual-ocaml/libref/Option.html) and [Haskell's Maybe](https://wiki.haskell.org/Maybe):
```
enum Option<T> {
    Some(T),
    None,
}
```

### The match Control Flow Operator
Similar to Ocaml's and Haskell's equivalent feature.

### Concise Control Flow with if let
With `if let` we can pattern match in an `if` expression:
```
    if let Some(3) = some_u8_value {
        println!("three");
    }
```

## Managing Growing Projects with Packages, Crates, and Modules
Modules are like C++ (class) namespaces, and use declarations are like C++ using directives.

 - A package can contain multiple binary crates and optionally one library crate.
 - A crate is a tree of modules that produces a library or executable.

### Packages and Crates
The _crate root_ is a source file that the Rust compiler starts from and makes up the root module of your crate.

A package contains a Cargo.toml file that describes how to build those crates.

Creating a binary (excutable) crate:
```
$ cargo new my-project
     Created binary (application) `my-project` package
$ ls my-project
Cargo.toml
src
$ ls my-project/src
main.rs
```

### Defining Modules to Control Scope and Privacy
Modules let us organize code within a crate into groups for readability and easy reuse. Modules also control the privacy of items, which is whether an item can be used by outside code (public) or is an internal implementation detail and not available for outside use (private).

Example showing nested modules:
```
mod front_of_house {
    mod hosting {
        fn add_to_waitlist() {}

        fn seat_at_table() {}
    }

    mod serving {
        fn take_order() {}

        fn serve_order() {}

        fn take_payment() {}
    }
}
```

### Paths for Referring to an Item in the Module Tree
A path can take two forms:

 - An _absolute path_ starts from a crate root by using a crate name or a literal `crate`.
 - A _relative path_ starts from the current module and uses `self`, `super`, or an identifier in the current module.
 - Double colon (::) is the path separator

In the following example:
 - Public function `eat_at_restaurant` is part of the public interface to this module, but private module `front_of_house` is not.
 - Function `eat_at_restaurant` can use module `front_of_house`, because they are defined in the same module
 - Function `eat_at_restaurant` can use module `front_of_house::hosting`, because `hosting` is public within `front_of_house`
 - Function `eat_at_restaurant` can use function `add_to_waitlist`, because `add_to_waitlist` is public within `hosting` and more generally because it can use every component of the path `front_of_house::hosting::add_to_waitlist`
```
mod front_of_house { 
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();
}
```

Not all of a structs fields need to have the same visibility; designating a struct as public does not make all its fields public. Each field must be separately designated as public. Designating an enum as public makes all its variants public.

### Bringing Paths into Scope with the use Keyword
Use declarations bring names inside a module into scope (like C++ using directives) `use std;`, optionally under a new name `use std as s;`. Names can also be re-exported with `pub use name;`.

External dependencies should be added to the `[dependencies]` section in the `Cargo.toml` file.

We can use a nested path at any level in a path, which is useful when combining two use statements that share a subpath:
```
use std::io; // this path can be
use std::io::Write; // combined with this one

use std::io::{self, Write}; // this combines both previous paths
```
If we want to bring all public items defined in a path into scope, we can use the glob operator (`*`):
```
use std::collections::*;
```

### Separating Modules into Different Files
We can move module `front_of_house` into its own file `src/front_of_house.rs`, and then bring it into scope using a module declaration `mod front_of_house;`.

## Common Collections
Vector, string and hash map.

### Storing Lists of Values with Vectors
Vectors are similar to vectors in C++ (and lists in Python).

### Storing UTF-8 Encoded Text with Strings
Strings are similar to strings in modern Python. We already mentioned `push_str` and `push` for concatenating strings and characters respectively.

Concatenate strings with `+`:
```
let s1 = String::from("Hello, ");
let s2 = String::from("world!");
let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used
```
Format strings with the `format!` macro:
```
let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");

let s = format!("{}-{}-{}", s1, s2, s3);
```
Due to strings using UTF-8, you cannot simply index a string's underlying `[u8]` and expect to get a valid character. Therefore strings cannot be indexed. String slices similarly use their indices for the underlying `[u8]`, but are allowed, but it is your own responsibility to make sure the bounds fall on character boundaries or risk a panic!

Getting grapheme clusters from strings is complex, so this functionality is not provided by the standard library, but supposedly crates for this task exist.

### Storing Keys with Associated Values in Hash Maps
A HashMap is very similar to a Python dictionary or a C++ map. By default, HashMap uses a [cryptographically strong hashing function](https://www.131002.net/siphash/siphash.pdf) that can provide resistance to Denial of Service (DoS) attacks. 
Example syntax:
```
use std::collections::HashMap;

let mut scores = HashMap::new();
scores.insert(String::from("Blue"), 10);

scores.entry(String::from("Yellow")).or_insert(50);
scores.entry(String::from("Blue")).or_insert(50);

println!("{:?}", scores);
```

## Error Handling
Errors are recoverable (like a missing file) or unrecoverable. An unrecoverable error is always a symptom of a software defect (like trying to access a location beyond the end of an array). Recoverable errors can be handled with the Result<OkType, ErrorType> type and unrecoverable errors should lead a call to the `panic!` macro.

### Unrecoverable Errors with `panic!`
When the `panic!` macro executes, your program will print a failure message, unwind and clean up the stack, and then quit. If you need to make an executable as small as possible, you can switch from unwinding to aborting upon a panic by adding `panic = 'abort'` to the appropriate `[profile]` sections in your _Cargo.toml_ file.

### Recoverable Errors with `Result`
The `Result` type is defined like so:
```
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```
Usage example:
```
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
}
```

The `?` operator can be used after a Result expression to get at the Ok value or return the Err value (through the `From::from` function).

If you use `?` in the main function its return type needs to be changed to `Result<(), Box<dyn Error>>` (for now read Box<dyn Error> as "any kind of error".):
```
use std::error::Error;
use std::fs::File;

fn main() -> Result<(), Box<dyn Error>> {
    let f = File::open("hello.txt")?;

    Ok(())
}
```

### To `panic!` or Not to `panic!`
Example of using a struct with a private field, but a public constructor to implement a value constraint:
```
pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess { value }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}
```
You should still check user input for conformance to constraints and output a meaningful message to the user.

## Generic Types, Traits, and Lifetimes

### Generic Data Types
Generic types are most similar to templates in C++ (no performance loss due to code duplication as needed) and C#/Java generics.

### Traits: Defining Shared Behavior
Traits are similar to interfaces in other languages and can be used to restrict generic types. You can also think of them as duck types: if it walks like a duck and quacks like a duck, then it must be a duck!

Defining and implementing a Trait:
```
pub trait Fly { fn fly(&self) -> String; }
pub trait FlyDefault { fn fly(&self) -> String { panic!() } }

struct Cat {pub name : String}

impl Fly for Cat {
    fn fly(&self) -> String { format!("Cat {} went flying!", self.name) }
}

fn main() {
    let garfield = Cat { name : "Garfield".to_string() };
    println!("{}", garfield.fly());
}
```

It is not possible to call the default implementation from an overriding implementation of that same method.

Using a trait bound on a generic type:
```
pub fn double_fly<F: Fly>(f: &F) -> String {
    f.fly() + &f.fly()
}
```
Or use multiple bounds: `<F: Fly + Crawl>` or use a where clause:
```
fn f<F>() -> ()
    where F: Fly + Crawl {}
```

### Validating References with Lifetimes
Every reference has a lifetime, which is the scope for which that reference is valid. The concept of lifetimes is somewhat different from tools in other programming languages, arguably making lifetimes Rust’s most distinctive feature. The main aim of lifetimes is to prevent dangling references. The lifetime elision rules allow some limited number of predictable deterministic patterns of lifetime annotations to be inferred by the compiler.

 - Each reference parameter gets its own lifetime.
 - If there is exactly one input lifetime, all output lifetimes equal that lifetime.
 - If there are multiple input lifetimes in a method, the lifetime of self is assigned to all output lifetime parameters.

 All string literals have the 'static lifetime, which we can annotate as follows: `
let s: &'static str = "I have a static lifetime.";`.

Example code:
```
use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(
    x: &'a str,
    y: &'a str,
    ann: T,
) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

## Writing Automated Tests
### How to Write Tests
At its simplest, a test in Rust is a function that’s annotated with the `test` attribute. To change a function into a test function, add `#[test]` on the line before `fn`. Useful macros: `assert!`, `assert_eq!`, `assert_ne!`. Useful annotations: `#[should_panic]`, `#[should_panic(expected = "expected substring of actual panic message")]`

Example code:
```
pub fn add_two(a: i32) -> i32 {
    a + 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_adds_two() {
        assert_eq!(4, add_two(2));
    }
}
```
Or:
```
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("two plus two does not equal four"))
        }
    }
}
```

### Controlling How Tests Are Run
 - Run tests consecutively with `cargo test -- --test-threads=1`.
 - Run tests with output of succeeding tests also shown with `cargo test -- --show-output`.
 - Mark expensive tests with the `#[ignore]` annotation and run them with `cargo test -- --ignored`.

### Test Organization
The `#[cfg(test)]` annotation on the `tests` module indicates that the test code should only run when you run `cargo test`, not when you run `cargo build`.

To create integration tests, you first need a top level _tests_ directory (next to _src_). We don’t need to annotate any code in tests/integration_test.rs with `#[cfg(test)]`; this is implicit.

Run only a specific integration test file with `cargo test --test integration_test`.

To abstract code in the tests directory into a common setup module use the file path tests/common/mod.rs instead of tests/common.rs.

## An I/O Project: Building a Command Line Program
Recapping what we've discussed by implementing a simple grep tool called minigrep.

### Accepting Command Line Arguments
Desired invocation is `cargo run searchstring example-filename.txt`. Sample code to read and print command line arguments:
```
use std::env;

fn main() {
    // will panic if arguments are not valid unicode strings
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);
}
```
To accept non-unicode strings there is `OsString` and `std::env::args_os`.

### Reading a File
`let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");`

### Refactoring to Improve Modularity and Error Handling
The following repsonsibilities should remain in `main`, and the rest should be moved to _lib.rs_.
 - Calling the command line parsing logic with the argument values
 - Setting up any other configuration
 - Calling a run function in lib.rs
 - Handling the error if run returns an error
This pattern is about separating concerns: _main.rs_ handles running the program, and _lib.rs_ handles all the logic of the task at hand.

`Box<dyn Error>` means a type that implements the Error trait.

### Developing the Library’s Functionality with Test-Driven Development

### Working with Environment Variables
```
pub fn search_case_insensitive<'a>(
    query: &str,
    contents: &'a str,
) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }

    results
}
```

```
impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 3 {
            return Err("not enough arguments");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
}
```

### Writing Error Messages to Standard Error Instead of Standard Output
Use `eprintln!` to write to the standard error port.

## Functional Language Features: Iterators and Closures
Closures and iterators.

### Closures: Anonymous Functions that Can Capture Their Environment
Closure syntax most closely resembles that of Smalltalk and Ruby:
```
fn  add_one_v1   (x: u32) -> u32 { x + 1 }
let add_one_v2 = |x: u32| -> u32 { x + 1 };
let add_one_v3 = |x|             { x + 1 };
let add_one_v4 = |x|               x + 1  ;
```
Closures don’t require you to annotate the types of the parameters or the return value like fn functions do. Closures are proper inline functions that always capture their full environment (unlike C++ closures where you have to specify capture, and unlike Python lambdas which must be a single expression).

Closures can capture values from their environment in three ways, which directly map to the three ways a function can take a parameter: taking ownership, borrowing mutably, and borrowing immutably. These are encoded in the three Fn traits as follows:

 - `FnOnce` consumes the variables it captures.
 - `FnMut` can change the environment because it mutably borrows values.
 - `Fn` borrows values from the environment immutably.

If you want to force the closure to take ownership of the values it uses in the environment, you can use the `move` keyword before the parameter list. This technique is mostly useful when passing a closure to a new thread to move the data so it’s owned by the new thread.

### Processing a Series of Items with Iterators
Rust has iterators like C++ and Python. Example code:
```
let v1 = vec![1, 2, 3];

let v1_iter = v1.iter();

for val in v1_iter {
    println!("Got: {}", val);
}
```
Iterator trait:
```
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;

    // methods with default implementations elided
}
```
The `iter` method produces an iterator over immutable references, `iter_mut` one over mutable references, and`into_iter` one over owned values.

Iterators implement methods `map` and `filter`.

### Improving Our I/O Project
Using iterators to improve the code.

### Comparing Performance: Loops vs. Iterators
Iterators are zero-cost abstractions.

## More About Cargo and Crates.io

### Customizing Builds with Release Profiles
With `cargo build`, you can do a build optimized for development (according to the `dev` profile). With `cargo build --release` you can do a build optimized for release (according to the `release` profile). By default the only difference is the level of optimization (opt-level), which is 0 for dev and 3 for release. 

By adding `[profile.*]` sections in _Cargo.toml_ you can control all settings of these profiles and define your own. For example:
```
[profile.dev]
opt-level = 1
```

### Publishing a Crate to Crates.io
You can generate HTML documentation, meant to help programmers _use_ your crate, from your source files with documentation comments. These start with three slashes (`///`), and are placed just before what you are documenting. They support Markdown notation. Example:
```
/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}
```
An added benefit is that the example code in this documentation doubles as a test that is run when you do `cargo test`!

You can use `//!` comments to document a containing item. This is most useful as top level comments to describe your whole crate (in lib.rs).

Generated documentation will also list any `pub use` reexported names.

### Cargo Workspaces
Workspaces are collections of packages that must work together.
See [chapter 14.3 in the book](https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html).

### Installing Binaries from Crates.io with cargo install
See [chapter 14.4 in the book](https://doc.rust-lang.org/book/ch14-04-installing-binaries.html).

### Extending Cargo with Custom Commands
Executables in your `$PATH` called `rust-something` act like rust subcommands.

## Smart Pointers
Smart pointers are like those in C++. References are pointers that only borrow data, but smart pointers can allow multiple owners. `String` and `Vec<T>` are smart pointers. A smart pointer is a struct that implements the `Deref` (like `operator->`) and `Drop` (destructor) traits. Examples:
 - `Box<T>` for allocating a `T` on the heap, like `unique_ptr<T>` in C++
 - `Rc<T>`, a reference-counting smart pointer to a `T` that allows multiple owners, like `shared_ptr<T>` in C++
 - `Ref<T>` and `RefMut<T>`, accessed through `RefCell<T>`, enforces borrowing rules at runtime instead of at compile time (similar to `ref` in SML/OCaml)

### Using `Box<T>` to Point to Data on the Heap
Uses:
 - to use a type whose size can’t be known at compile time in a context that requires an exact size
 - to ensure transfer of ownership of a large amount of data will not copy that data around the stack
 - to use a value that implements a particular trait rather than being of a specific type
Example code:
```
enum List {
    Cons(i32, Box<List>),
    Nil,
}

use crate::List::{Cons, Nil};

fn main() {
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
}
```

### Treating Smart Pointers Like Regular References with the Deref Trait
Deref coercion happens in thesethree cases:
 - from `&T` to `&U` when `T: Deref<Target=U>`,
 - from `&mut T` to `&mut U` when `T: DerefMut<Target=U>`,
 - from `&mut T` to `&U` when `T: Deref<Target=U>`.

### Running Code on Cleanup with the `Drop` Trait
To manually call `drop`, use the `drop` function (not method).

### `Rc<T>`, the Reference Counted Smart Pointer
`Rc<T>` is only for use in single-threaded scenarios. Example code:
```
enum List {
    Cons(i32, Rc<List>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::rc::Rc;

fn main() {
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    let b = Cons(3, Rc::clone(&a));
    let c = Cons(4, Rc::clone(&a));
}
```

### `RefCell<T>` and the Interior Mutability Pattern
`RefCell<T>` (similar to `ref` in SML/OCaml) is for code that follows the borrowing rules, even though the borrow checker is unable to prove that. Or simply because you want to convert an immutable borrow into a mutable one!
`RefCell<T>` is only for use in single-threaded scenarios. Example code:
```
use std::cell::RefCell;

fn invalid_mutate_immutable() {
    let x = 1;
    // x = 0; // uncomment to assign to immutable variable
    println!("x = {}", x);
}

fn valid_mutate_immutable() {
    let x = RefCell::new(1);
    *x.borrow_mut() = 0; // assign to refcell
    println!("x = {}", x.borrow());
}

fn main() {
    invalid_mutate_immutable();
    valid_mutate_immutable();
}
```

`RefCell`s are often used in combination with `Rc`s:
```
#[derive(Debug)]
enum List<T> {
    Cons(Rc<RefCell<T>>, Rc<List<T>>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::cell::RefCell;
use std::rc::Rc;

fn main() {
    let value = Rc::new(RefCell::new(5));

    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

    let b = Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
    let c = Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));

    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);
}

```

A similar type is `Cell<T>` which does not give references like `RefCell<T>`, but which copies the value into and out of the `Cell<T>`. A thread-safe variant of `RefCell<T>` is `Mutex<T>`.

### Reference Cycles Can Leak Memory
Using `RefCell`s it is possible to create circular memory references which keep themselves alive even though there are no outside references any more. The solution is to use a `Weak<T>`. You can go from a `RefCell` to a `~`Weak` with `.downgrade()` and back with `.upgrade()`.

## Fearless Concurrency
A major goal of Rust is handling concurrent programming safely and efficiently: _fearless concurrency_. (Concurrent programming is decomposing a program into partially-ordered parts. Concurrent programming enables parallel execution.)

### Using Threads to Run Code Simultaneously
Example code:
```
use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap(); // unwrap, so panic in case of error result
}
```
Taking ownership of a vector:
```
use std::thread;

fn main() {
    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        println!("Here's a vector: {:?}", v);
    });

    handle.join().unwrap();
}
```

### Using Message Passing to Transfer Data Between Threads
Asynchronous message passing is the approach taken to safe concurrency by languages such as Go (unbuffered channels) and Erlang (buffered asynchronous channels) (but see Concurrent ML for how to do it right).
Example code:
```
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel(); // multiple producer, single consumer

    let tx1 = tx.clone();
    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}
```

### Shared-State Concurrency
A `Mutex<T>` gives mutual exclusion to a value of type `T`. Beware of deadlocks! Example code:
```
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
```

### Extensible Concurrency with the Sync and Send Traits
Types for which it is safe to transfer ownership to another thread are marked as such by having the trait `Send`.
Types for which it is safe to be referenced from multiple threads are marked as such by having the trait `Sync`. Thus type `T` is `Sync` if `&T` is `Send`.

## Object Oriented Programming Features of Rust
### Using Trait Objects That Allow for Values of Different Types
Dynamic dispatch is supported using trait objects. Example code:
```
pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}
```
For a trait to be used this way its methods must not return `Self` or have generic type paramters. 

### Implementing an Object-Oriented Design Pattern
An example of a blog post going through various states implemented using the state pattern. This code is non-idiomatic and you are better off using the typed functional features.

## Patterns and Matching
### All the Places Patterns Can Be Used
Patterns are valid in match arms, `if let`, `while let`, `for` loops, `let`, function parameters.

### Refutability: Whether a Pattern Might Fail to Match
Some patterns can fail to match the expression they are matched against and others will always match. Depending on the context you will need one or the other.

### Pattern Syntax
Pattern matching seems to be very similar to Haskell's including @-bindings.

Variables whose name starts with an underscore (_) do not trigger unused variable warnings. This can be used as part of a pattern. You can also ignore multiple remaining parts with `..`.

## Advanced Features
### Unsafe Rust
In an `unsafe` block some safety features are relaxed, so more responsibilty rests with the programmer to ensure valid code. In an `unsafe` block you may:
 - dereference a raw pointer
 - call an unsafe function or method
 - access or modify a mutable static variable
 - implement an unsafe trait
 - access fields of unions

All other safety features including the borrow checker continue to work, but raw pointers are not subject to borrow checking.

#### Dereferencing a Raw Pointer
Raw pointers are like pointers in C and C++. They are either immutable `*const T` (pointer to constant) or mutable `*mut T`. Raw pointers are exempt from the borrowing rules and can be null or otherwise invalid.

You can create raw pointers in safe code, but only dereference them in unsafe blocks:
```
let mut num = 5;

let r1 = &num as *const i32;
let r2 = &mut num as *mut i32;

unsafe {
    println!("r1 is: {}", *r1);
    println!("r2 is: {}", *r2);
}
```

#### Calling an Unsafe Function or Method
If you mark a function or method as `unsafe` (`unsafe fn dangerous() {}`), then its body effectively becomes an `unsafe` block, and calls to it need to be in `unsafe` blocks.

You declare functions following the C language standard calling conventions inside an `extern "C"` block. Such functions are automatically also unsafe, so calls to them must be inside `unsafe` blocks. You can also make Rust functions callable via these conventions (and prevent the compiler from changing the name of the function in the assembly code):
```
#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("Just called a Rust function from C!");
}
```

#### Accessing or Modifying a Mutable Static Variable
Accessing or modifying a mutable static variable must happen inside an unsafe block.

The difference between immutable static variables and constants is that static variables have a fixed address in memory, while constants may be duplicated (such as happens when inlining a constant).

#### Implementing an Unsafe Trait
A trait is unsafe when its use (or use of its methods) is subject to conditions that the compiler can’t verify. Examples are invariants that need to be manually maintained, or the marker traits `Send` and `Sync`.

#### Accessing Fields of a Union
For interoperability with C and C++, Rust understands C-style unions, but accessing such a union is unsafe, because the compiler cannot verify which of the unions types is actual.

### Advanced Traits
#### Specifying Placeholder Types in Trait Definitions with Associated Types
If `Iterator` were defined as:
```
pub trait Iterator<T> {
    fn next(&mut self) -> Option<T>;
}
```
and we wanted to implement it for `Vec<U>`, then in principal we could define one for each combination of T and U. That is actually more general than we want. To implement an Iterator<T> for a Vec<T>, where there is only one type T, we can use an associated type in Iterator:
```
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;
}
```
and set it to the correct type T:
```
impl<T> Iterator for Vec<T> {
    type Item = T;
}
```
This way the compiler knows there can only ever be one implementation of Iterator for a specific type and we won't need to needlessly disambiguate the type.

#### Default Generic Type Parameters and Operator Overloading
Operator overloading works a bit like in Python: to overload the `+` operator you have to implement the Add:add method. Definition of Add trait:
```
trait Add<Rhs=Self> { // generic type with default
    type Output;

    fn add(self, rhs: Rhs) -> Self::Output;
}
```
Example code:
```
use std::ops::Add;

struct Millimeters(u32);
struct Meters(u32);

impl Add<Meters> for Millimeters {
    type Output = Millimeters;

    fn add(self, other: Meters) -> Millimeters {
        Millimeters(self.0 + (other.0 * 1000))
    }
}
```

#### Fully Qualified Syntax for Disambiguation: Calling Methods with the Same Name
If a type has multiple functions with the same name associated with it via direct implementation or via traits, then we can disambiguate with `<Type as Trait>::function(receiver_if_method, next_arg, ...);`.

#### Using Supertraits to Require One Trait’s Functionality Within Another Trait
Use `trait Trait : Bound` to put a trait bound on the `self` parameter of all default trait methods. The trait bound is called a supertrait for this trait.

#### Using the Newtype Pattern to Implement External Traits on External Types
Like in Haskell we can use `newtype` to create different types that are identical at runtime. We can use this to create a local copy of a type for which we can then implement external traits. For example to implement `Display` on `Vec<String>` we can do:
```
use std::fmt;

struct Wrapper(Vec<String>); // a ``newtype'' tuple struct (must have a single field)

impl fmt::Display for Wrapper { // implement Display on Vec<String>
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}]", self.0.join(", ")) // underlying type accessed with `self.0`
    }
}

fn main() {
    let w = Wrapper(vec![String::from("hello"), String::from("world")]);
    println!("w = {}", w);
}
```
If we wanted to forward all vector methods to the wrapper the Deref trait would come in handy.

### Advanced Types
Newtypes are useful for statically enforcing that certain values are not confused, to indicate units, or to hide implementation details. We can give a type another (shorter) name via `type other_type_name = some_type_name`, for example `type Thunk = Box<dyn Fn() + Send + 'static>;`.

The empty or bottom type (which cannot have values) is written `!` and can be used as the return type of functions that diverge. It can be coerced to any type. Examples that have empty type are `loop`, `continue`, `panic!`

The `Sized` trait is automatically defined for each type whose size is constant. Types that are not `Sized` can only be used indirectly via a reference or pointer type. Generic function parameters have an implicit bound on `Sized`, though this can be relaxed with a bound on `?Sized`.

### Advanced Functions and Closures
In addition to closures you can define function pointers, which are mainly for compatibility with C.

To return a closure, we can use a trait object:
```
fn returns_closure() -> Box<dyn Fn(i32) -> i32> {
    Box::new(|x| x + 1)
}
```

### Macros
There are two forms of macro: declarative macros (like Scheme's syntax-rules) and procedural macros (transformations of token streams (or abstract syntax trees (ASTs)). Procedural macros come in three variations: custom derive (`[#derive(MyMacro)]`), attribute-like (`#[my_macro(arguments)]`), and function-like (`my_macro!(arguments)`). Declarative macros only support the function-like variation for now. Unlike functions, macros must be defined before they are called. Procedural macros must be in their own crate (this restriction might be lifted in future).
Example simplified (non-preallocating) declarative `vec` macro:
```
#[macro_export] // export this macro
macro_rules! vec { // macro name without exclamation mark
    ( $( $x:expr ),* ) => { // first (and only) pattern (like in a match expression)
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}
```

Procedural macro custom derive example code:
```
extern crate proc_macro; // crate internal to the compiler

use proc_macro::TokenStream; // the input and output type for a procedural macro
use quote::quote; // to transform our code (like (quasi)quote from Lisp)
use syn; // to parse client code (text) into a TokenStream

#[proc_macro_derive(HelloMacro)] // invoke macro with #[derive(HelloMacro)]
pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap(); // parse code into an abstract syntax tree
    impl_hello_macro(&ast) // call function that does the real work
}

fn impl_hello_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl HelloMacro for #name {
            fn hello_macro() {
                println!("Hello, Macro! My name is {}!", stringify!(#name));
            }
        }
    };
    gen.into()
}
```

Procedural macro attribute-like example code:
```
#[proc_macro_attribute] // macro definition
pub fn route(attr: TokenStream, item: TokenStream) -> TokenStream { /* ... */ }

#[route(GET, "/")] // macro use
fn index() { /* ... */ }
```

Procedural macro function-like example code:
```
#[proc_macro] // macro definition
pub fn sql(input: TokenStream) -> TokenStream { /* ... */ }

let sql = sql!(SELECT * FROM posts WHERE id=1); // macro use
```

## Final Project: Building a Multithreaded Web Server
Developing a toy "Hello, world!" web server with compiler-driven development.
Server code:
```
use hello::ThreadPool;
use std::fs;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;
use std::time::Duration;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4);

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    }

    println!("Shutting down.");
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();

    let get = b"GET / HTTP/1.1\r\n";
    let sleep = b"GET /sleep HTTP/1.1\r\n";

    let (status_line, filename) = if buffer.starts_with(get) {
        ("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
    } else if buffer.starts_with(sleep) {
        thread::sleep(Duration::from_secs(5));
        ("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
    } else {
        ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
    };

    let contents = fs::read_to_string(filename).unwrap();

    let response = format!("{}{}", status_line, contents);

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}
```
Thread pool code:
```
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

impl ThreadPool {
    /// Create a new ThreadPool.
    ///
    /// The size is the number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending terminate message to all workers.");

        for _ in &self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down all workers.");

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let message = receiver.lock().unwrap().recv().unwrap();

            match message {
                Message::NewJob(job) => {
                    println!("Worker {} got a job; executing.", id);

                    job();
                }
                Message::Terminate => {
                    println!("Worker {} was told to terminate.", id);

                    break;
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}
```
## Apppendix
### Appendix A: Keywords
Conains a list of current and reserved keywords.
Ordinarily you cannot use a keyword as an identifier. You can get around this restriction using raw identifier syntax (prepend "r#" to the name). Example code:
```
fn r#match(needle: &str, haystack: &str) -> bool {
    haystack.contains(needle)
}

fn main() {
    assert!(r#match("foo", "foobar"));
}
```

### Appendix B: Operators and Symbols

### Appendix C: Derivable Traits
This appendix provides a reference of all derivable the traits in the standard library.

### Appendix D - Useful Development Tools

 - Automatic Formatting with rustfmt (rustup component add rustfmt => cargo fmt)
 - Fix Your Code with rustfix (included: cargo fix)
 - More Lints with Clippy (rustup component add clippy => cargo clippy)
 - IDE Integration Using the Rust Language Server (rustup component add rls)

### Appendix E - Editions
Rust has a six-week release cycle, but every two to three years, the Rust team produces a new Rust _edition_, collecting the changes from many releases. Current editions are: Rust 2015 (default) and Rust 2018. For more details see the [_Edition Guide_](https://doc.rust-lang.org/stable/edition-guide/).

### Appendix F: Translations of the Book

### Appendix G - How Rust is Made and Nightly Rust
You should never have to fear upgrading to a new version of stable Rust. Each upgrade should be painless, but should also bring you new features, fewer bugs, and faster compile times.

There are three release channels for Rust:
 - Nightly (automatic daily release)
 - Beta (a specific Nightly that is feature-frozen, but does receives fixes for 6 weeks)
 - Stable (a six week old Beta)

New features under active development are hidden behind a feature flag, and feature flags are only supported on nightly releases. Thus is _stability without stagnation_ accomplished.

For teams working on Rust see the [Rust governance page][https://www.rust-lang.org/governance].
